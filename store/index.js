export const state = () => ({
  todos: [],
})

export const mutations = {
  setTodos(state, todos) {
    state.todos = todos
  },
  add(state, todo) {
    state.todos.push(todo)
  },
  remove(state, _id) {
    state.todos = state.todos.filter(el => el._id !== _id)
  },
  edit(state, {_id, title, completed}) {
    let edited = state.todos.find(el => el._id == _id)
    edited.title = title
    edited.completed = completed
  }
}

export const actions = {
  async addTodo({ commit }, value) {
    try {
      const created = await this.$host.post('/api/todos', { title: value })
      commit('add', created.data)
    }catch(e) {
      console.log(e);
    }
  },
  async removeTodo({ commit }, _id) {
    try {
      await this.$host.delete(`/api/todos/${_id}`)
      commit('remove', _id)
    }catch(e) {
      console.log(e);
    }
  },
  async editTodo({ commit }, {_id, title, completed}) {
    try {
      await this.$host.put(`/api/todos/${_id}`, { title, completed })
      commit('edit', {_id, title, completed})
    }catch(e) {
      console.log(e);
    }
  },
  async fetchTodos({ commit }) {
    const todos = await this.$host.get('/api/todos')
    commit('setTodos', todos.data)
  }
}

export const getters = {
  todos: s => s.todos,
}